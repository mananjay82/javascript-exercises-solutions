function addWithSurcharge(a, b){
  let surCharge1, surCharge2;
  if(a <= 10){
    surCharge1 = 1;
  }
  else{
    surCharge1 = 2;
  }
  if(b <= 10) { surCharge2 = 1; }
  else { surCharge2 = 2; }
  
  return a + b + surCharge1 + surCharge2;
}
