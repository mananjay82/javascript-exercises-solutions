function addWithSurcharge(a, b){
  let surCharge1, surCharge2;
  if(a <= 10) { surCharge1 = 1; }
  else if(a > 10 && a <= 20) { surCharge1 = 2; }
  else { surCharge1 = 3; }
  
  if(b <= 10) { surCharge2 = 1; }
  else if(b > 10 && b <= 20) { surCharge2 = 2; }
  else { surCharge2 = 3; }
 
  return a + b + surCharge1 + surCharge2;
}
