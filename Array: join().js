function list(arr){
if(arr.length === 0){
return '';
}
if(arr.length === 1){
return arr[0];
}
let wordsExLast = arr.slice(0, arr.length-1);
let lastWord = arr[arr.length - 1];

return wordsExLast.join(', ') + ' and ' + lastWord;
}
