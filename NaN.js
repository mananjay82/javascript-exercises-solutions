function parseFirstInt(str){
let inputToParse = str;
for(let i = 0; i < str.length; i++){
let firstInt = parseInt(inputToParse);
if(!Number.isNaN(firstInt)){
return firstInt;
}
inputToParse = inputToParse.substr(1);
}
return NaN;
}
