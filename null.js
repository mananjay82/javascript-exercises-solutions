function cutComment(str){
if(str.indexOf('//') == -1){
return null;
}
let indexOfComment = str.indexOf('//');
return str.slice(indexOfComment+2, str.length).trim();
}
